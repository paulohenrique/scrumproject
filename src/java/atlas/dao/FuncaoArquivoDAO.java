package atlas.dao;

import atlas.modelo.FuncaoArquivo;
import br.com.caelum.vraptor.ioc.Component;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

/**
 * Clase de teste para uma DAO extendido apartir de uma DAO generico
 *
 * @author Maison Chaves
 */
@Component
public class FuncaoArquivoDAO extends AbstractDAO<FuncaoArquivo>
{
    /**
     *
     * @param session
     */
    public FuncaoArquivoDAO(Session session)
    {
        super(session);
    }

    /**
     *
     * @param nome
     * @return
     */
    public List<FuncaoArquivo> busca(String nome)
    {
        return getSession().createCriteria(FuncaoArquivo.class).add(Restrictions.ilike("descricao", nome, MatchMode.ANYWHERE)).list();
    }

    /**
     *
     * @param page
     * @param pageSize
     * @return
     */
    public List<FuncaoArquivo> busca(Integer page, Integer pageSize)
    {
        Criteria criteria = getSession().createCriteria(FuncaoArquivo.class);

        criteria.addOrder(Order.asc("descricao"));
        criteria.setFirstResult((page - 1) * pageSize);
        criteria.setMaxResults(pageSize);
        return criteria.list();
    }

    /**
     * Checa se não existe outra funcao como o mesmo nome excuindo ela
     * propria.
     *
     * @param funcao
     * @return
     */
    public Boolean check(FuncaoArquivo funcao)
    {
        Criteria check = getSession().createCriteria(FuncaoArquivo.class);
        if (funcao != null)
        {
            if (funcao.getDescricao() != null && !funcao.getDescricao().equals(""))
            {
                check.add(Restrictions.like("descricao", funcao.getDescricao(), MatchMode.ANYWHERE));
            }
            if (funcao.getId()!= null && funcao.getId() != 0)
            {
                check.add(Restrictions.ne("id", funcao.getId()));
            }
        }

        return check.list().isEmpty();
    }
}
