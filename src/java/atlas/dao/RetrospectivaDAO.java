package atlas.dao;

import atlas.modelo.ItemRetrospectiva;
import br.com.caelum.vraptor.ioc.Component;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

/**
 * Clase de teste para uma DAO extendido apartir de uma DAO generico
 *
 * @author Maison Chaves
 */
@Component
public class RetrospectivaDAO extends AbstractDAO<ItemRetrospectiva>
{
    /**
     *
     * @param session
     */
    public RetrospectivaDAO(Session session)
    {
        super(session);
    }

    /**
     *
     * @param nome
     * @return
     */
    public List<ItemRetrospectiva> busca(String nome)
    {
        return getSession().createCriteria(ItemRetrospectiva.class).add(Restrictions.ilike("descricao", nome, MatchMode.ANYWHERE)).list();
    }

    /**
     *
     * @param page
     * @param pageSize
     * @return
     */
    public List<ItemRetrospectiva> busca(Integer page, Integer pageSize)
    {
        Criteria criteria = getSession().createCriteria(ItemRetrospectiva.class);

        criteria.addOrder(Order.asc("descricao"));
        criteria.setFirstResult((page - 1) * pageSize);
        criteria.setMaxResults(pageSize);
        return criteria.list();
    }
}
