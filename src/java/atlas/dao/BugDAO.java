package atlas.dao;

import atlas.modelo.Bug;
import br.com.caelum.vraptor.ioc.Component;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

/**
 * Clase de teste para uma DAO extendido apartir de uma DAO generico
 *
 * @author Maison Chaves
 */
@Component
public class BugDAO extends AbstractBuscaDAO<Bug>
{
    /**
     *
     * @param session
     */
    public BugDAO(Session session)
    {
        super(session);
    }

//    /**
//     *
//     * @param numero
//     * @return
//     */
//    public List<Bug> busca(Integer numero)
//    {
//        return getSession().createCriteria(Bug.class).add(Restrictions.eq("numero", numero)).list();
//    }

    /**
     *
     * @param page
     * @param pageSize
     * @return
     */
    public List<Bug> busca(Integer page, Integer pageSize)
    {
        Criteria criteria = getSession().createCriteria(Bug.class);

        criteria.addOrder(Order.desc("titulo"));
        criteria.setFirstResult((page - 1) * pageSize);
        criteria.setMaxResults(pageSize);
        return criteria.list();
    }

    /**
     * Checa se não existe outra bug como o mesmo nome excuindo ela
     * propria.
     *
     * @param bug
     * @return
     */
    public Boolean check(Bug bug)
    {
        Criteria check = getSession().createCriteria(Bug.class);
        if (bug != null)
        {
            if (bug.getTitulo() != null)
            {
                check.add(Restrictions.eq("titulo", bug.getTitulo()));
            }
            if (bug.getId() != null && bug.getId() != 0)
            {
                check.add(Restrictions.ne("id", bug.getId()));
            }
        }

        return check.list().isEmpty();
    }

    @Override
    protected Criteria criteriosBusca(Bug bug, Criteria criteria)
    {
        if (bug != null)
        {
            if (bug.getTitulo()!= null && !bug.getTitulo().equals(""))
            {
                criteria.add(Restrictions.eq("titulo", bug.getTitulo()));
            }
            if (bug.getUrl()!= null && !bug.getUrl().equals(""))
            {
                criteria.add(Restrictions.eq("url", bug.getUrl()));
            }
        }
        return criteria;
    }

    @Override
    protected Criteria ordem(Criteria criteria)
    {
        return criteria.addOrder(Order.desc("titulo"));
    }
}
