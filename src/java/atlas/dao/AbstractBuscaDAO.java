package atlas.dao;

import br.com.caelum.vraptor.ioc.Component;
import java.io.Serializable;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;

/**
 * Classe de DAO generico para dao's que implementam busca
 *
 * @param <T> 
 * @author Maison Chaves
 */
@Component
public abstract class AbstractBuscaDAO<T extends Serializable> extends AbstractDAO<T>
{
    /**
     * Construtor padrão
     * @param session
     */
    protected AbstractBuscaDAO(Session session)
    {
        super(session);
    }

    /**
     * Recupera os objetos com base nos criterios de busca
     * @param objeto
     * @return
     */
    public List<T> busca(T objeto)
    {
        Criteria criteria = getSession().createCriteria(getPersistentClass());
        criteria = criteriosBusca(objeto, criteria);
        criteria = ordem(criteria);
        return criteria.list();
    }

    /**
     * Recupera os objetos paginados com base nos criterios de busca
     * @param objeto
     * @param page
     * @param pageSize
     * @return
     */
    public List<T> busca(T objeto, Integer page, Integer pageSize)
    {
        Criteria criteria = getSession().createCriteria(getPersistentClass());
        criteria = criteriosBusca(objeto, criteria);
        criteria = ordem(criteria);
        criteria.setFirstResult((page -1) * pageSize);
        criteria.setMaxResults(pageSize);
        return criteria.list();
    }

    /**
     * Recupera os total de registros com base nos criterios de busca
     * @param objeto
     * @return
     */
    public Integer getTotalRegistros(T objeto)
    {
        Criteria criteria = getSession().createCriteria(getPersistentClass());
        criteria = criteriosBusca(objeto, criteria);
        criteria.setProjection(Projections.rowCount());
        return (Integer) criteria.uniqueResult();
    }

    /**
     * Define os criteirios de busca para os metodos padrões
     * @param objeto objeto a ser usado nos filtros
     * @param criteria criteiria usada nos filtros
     * @return
     */
    abstract protected Criteria criteriosBusca(T objeto, Criteria criteria);
    
    /**
     * Define a ordem padrão de ordenação
     */
    abstract protected Criteria ordem(Criteria criteria);
}
