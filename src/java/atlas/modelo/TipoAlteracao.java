
package atlas.modelo;

/**
 *
 * @author Maison Chaves
 */
public enum TipoAlteracao
{
    A("arquivo"), T("tabela");
    private String descricao;

    private TipoAlteracao(String descricao)
    {
        this.descricao = descricao;
    }

    public String getDescricao()
    {
        return descricao;
    }
}
