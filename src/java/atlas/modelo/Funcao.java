
package atlas.modelo;

/**
 *
 * @author Maison Chaves
 */
public enum Funcao
{
    PO("Product Owner"), SM("Scrum Master"), MT("Time Member");
    private String descricao;

    private Funcao(String descricao)
    {
        this.descricao = descricao;
    }

    public String getDescricao()
    {
        return descricao;
    }
}
