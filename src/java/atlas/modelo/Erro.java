
package atlas.modelo;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author Maison Chaves
 */
@Entity
public class Erro implements Serializable
{
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String descricao;
    @Enumerated(EnumType.STRING)
    private StatusErro tipo;
    @ManyToOne
    @JoinColumn(name = "membroId")
    private Membro relator;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getDescricao()
    {
        return descricao;
    }

    public void setDescricao(String descricao)
    {
        this.descricao = descricao;
    }

    public StatusErro getTipo()
    {
        return tipo;
    }

    public void setTipo(StatusErro tipo)
    {
        this.tipo = tipo;
    }

    public Membro getRelator()
    {
        return relator;
    }

    public void setRelator(Membro relator)
    {
        this.relator = relator;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Erro))
        {
            return false;
        }
        Erro other = (Erro) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "atlas.modelo.Erro[ id=" + id + " ]";
    }
    
}
