
package atlas.modelo;

/**
 *
 * @author Maison
 */
public enum Nota {
    MR("Muito Ruim"), R("Ruim"), M("Medio"), B("Bom"), MB("Muito Bom");
    private String descricao;

    private Nota(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }
    
}
