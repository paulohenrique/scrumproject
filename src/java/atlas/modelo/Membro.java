package atlas.modelo;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

/**
 *
 * @author Maison Chaves
 */
@Entity
public class Membro implements Serializable
{
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String nome;
    private Float horas;
    @Enumerated(EnumType.STRING)
    private Funcao funcao;
    @ManyToMany(mappedBy = "membros")
    private List<Sprint> sprints;
    @OneToMany(mappedBy = "membro")
    private List<ControleTempo> registrosTempo;
    @OneToMany(mappedBy = "relator")
    private List<Erro> erros;
    @OneToMany(mappedBy = "membro")
    private List<Tarefa> tarefas;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="membroId")
    private Login login;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNome()
    {
        return nome;
    }

    public void setNome(String nome)
    {
        this.nome = nome;
    }

    public Float getHoras()
    {
        return horas;
    }

    public void setHoras(Float horas)
    {
        this.horas = horas;
    }

    public Funcao getFuncao()
    {
        return funcao;
    }

    public void setFuncao(Funcao funcao)
    {
        this.funcao = funcao;
    }

    public List<Sprint> getSprints()
    {
        return sprints;
    }

    public void setSprints(List<Sprint> sprints)
    {
        this.sprints = sprints;
    }

    public void setTarefas(List<Tarefa> tarefas)
    {
        this.tarefas = tarefas;
    }

    public List<Tarefa> getTarefas()
    {
        return tarefas;
    }

    public List<ControleTempo> getRegistrosTempo()
    {
        return registrosTempo;
    }

    public void setRegistrosTempo(List<ControleTempo> registrosTempo)
    {
        this.registrosTempo = registrosTempo;
    }

    public List<Erro> getErros()
    {
        return erros;
    }

    public void setErros(List<Erro> erros)
    {
        this.erros = erros;
    }

    public Login getLogin() {
        return login;
    }

    public void setLogin(Login login) {
        this.login = login;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Membro))
        {
            return false;
        }
        Membro other = (Membro) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "atlas.modelo.Membro[ id=" + id + " ]";
    }
}
