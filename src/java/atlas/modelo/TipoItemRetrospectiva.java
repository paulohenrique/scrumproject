
package atlas.modelo;

/**
 *
 * @author Maison Chaves
 */
public enum TipoItemRetrospectiva
{
    B("Bom"), R("Ruim"), F("Passar a Fazer");
    private String descricao;

    private TipoItemRetrospectiva(String descricao)
    {
        this.descricao = descricao;
    }

    public String getDescricao()
    {
        return descricao;
    }
}
