
package atlas.infra;

import br.com.caelum.vraptor.ioc.Component;
import br.com.caelum.vraptor.ioc.ComponentFactory;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 *
 * @author Maison Chaves
 */
@Component
public class SessionCreator implements ComponentFactory<Session>
{
    private final SessionFactory factory;
    private Session session;

    /**
     * Inicia a fabrica de sessoes
     * @param factory
     */
    public SessionCreator(SessionFactory factory)
    {
        this.factory = factory;
    }

    /**
     * Inicia a sessao
     */
    @PostConstruct
    public void abre()
    {
        this.session = factory.openSession();
    }

    /**
     * recuepra a sessão aberta
     * @return
     */
    @Override
    public Session getInstance()
    {
        return this.session;
    }

    /**
     * fecha a sessao
     */
    @PreDestroy
    public void fecha()
    {
        this.session.close();
    }
}
