
package atlas.infra.Annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * Ideintifica metodos de acesso com login
 * @author Maison Chaves
 */
@Retention(RetentionPolicy.RUNTIME) //a anotação vai ficar disponível em tempo de execucao
@Target(ElementType.METHOD) // anotação para métodos
public @interface Logado
{
}