
package atlas.cotrole;
import atlas.dao.RetrospectivaDAO;
import atlas.modelo.ItemRetrospectiva;
import atlas.modelo.TipoItemRetrospectiva;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.validator.ValidationMessage;
import java.util.Arrays;
import java.util.List;

/**
 * @author Maison Chaves
 */
@Resource
public class RetrospectivaController
{
    private final RetrospectivaDAO dao;
    private final Result result;
    private final Validator validator;

    /**
     *
     * @param itemRetrospectivaDAO
     * @param result
     * @param validator
     */
    public RetrospectivaController(RetrospectivaDAO itemRetrospectivaDAO, Result result, Validator validator)
    {
        this.validator = validator;
        this.result = result;
        this.dao = itemRetrospectivaDAO;
    }

    /**
     *
     * @param page
     * @param pageSize
     * @return
     */
    public List<ItemRetrospectiva> lista(Integer page, Integer pageSize)
    {
        if (page == null || pageSize == null)
        {
            page = 1;
            pageSize = 10;
        }

        Integer total = dao.getTotalRegistros();

        result.include("page", page);
        result.include("pageSize", pageSize);
        result.include("total", total);
        result.include("totalPage", ((total - 1) / pageSize) + 1);
        return dao.busca(page, pageSize);
    }

    /**
     *
     * @param itemRetrospectiva
     */
    public void adiciona(final ItemRetrospectiva itemRetrospectiva)
    {
        if (itemRetrospectiva.getDescricao()== null || itemRetrospectiva.getDescricao().length() < 3)
        {
            validator.add(new ValidationMessage("Descrição é obrigatório e precisa ter mais de 3 letras", "produto.nome"));
        }
        validator.onErrorUsePageOf(this).formulario();

        dao.salva(itemRetrospectiva);
        result.redirectTo(this).lista(null,null);
    }

    /**
     *
     * @param id
     * @return
     */
    public ItemRetrospectiva edita(Long id)
    {
        result.include("tipoItemRetrospectivaList", Arrays.asList(TipoItemRetrospectiva.values()));
        return dao.carrega(id);
    }

    /**
     *
     * @param itemRetrospectiva
     */
    public void altera(ItemRetrospectiva itemRetrospectiva)
    {
        if (itemRetrospectiva.getDescricao()== null || itemRetrospectiva.getDescricao().length() < 3)
        {
            validator.add(new ValidationMessage("Nome é obrigatório e precisa ter mais de 3 letras", "produto.nome"));
        }
        validator.onErrorUsePageOf(this).edita(itemRetrospectiva.getId());

        dao.altera(itemRetrospectiva);
        result.redirectTo(this).lista(null,null);
    }

    /**
     *
     * @param id
     */
    public void remove(Long id)
    {
        ItemRetrospectiva itemRetrospectiva = dao.carrega(id);
        dao.remove(itemRetrospectiva);
        result.redirectTo(this).lista(null,null);
    }

    /**
     *
     */
    public List<TipoItemRetrospectiva> formulario()
    {
        return Arrays.asList(TipoItemRetrospectiva.values());
    }
}
