
package atlas.cotrole;
import atlas.dao.DiretorioDAO;
import atlas.dao.ModuloDAO;
import atlas.modelo.Diretorio;
import atlas.modelo.Nota;
import atlas.modelo.TipoPacote;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.validator.ValidationMessage;
import java.util.Arrays;
import java.util.List;

/**
 * @author Maison Chaves
 */
@Resource
public class DiretorioController
{
    private final DiretorioDAO dao;
    private final ModuloDAO moduloDAO;
    private final Result result;
    private final Validator validator;

    /**
     *
     * @param diretorioDAO
     * @param result
     * @param validator
     */
    public DiretorioController(DiretorioDAO diretorioDAO, ModuloDAO moduloDAO, Result result, Validator validator)
    {
        this.validator = validator;
        this.result = result;
        this.dao = diretorioDAO;
        this.moduloDAO = moduloDAO;
    }

    /**
     *
     * @param page
     * @param pageSize
     * @return
     */
    public List<Diretorio> lista(Integer page, Integer pageSize)
    {
        if (page == null || pageSize == null)
        {
            page = 1;
            pageSize = 10;
        }

        Integer total = dao.getTotalRegistros();

        result.include("page", page);
        result.include("pageSize", pageSize);
        result.include("total", total);
        result.include("totalPage", ((total - 1) / pageSize) + 1);
        return dao.busca(page, pageSize);
    }

    /**
     *
     * @param diretorio
     */
    public void adiciona(final Diretorio diretorio)
    {
        if (diretorio.getNome() == null || diretorio.getNome().length() < 3)
        {
            validator.add(new ValidationMessage("Nome é obrigatório e precisa ter mais de 3 letras", "produto.nome"));
        }
        validator.onErrorUsePageOf(this).formulario();

        dao.salva(diretorio);
        result.redirectTo(this).lista(null,null);
    }

    /**
     *
     * @param id
     * @return
     */
    public Diretorio edita(Long id)
    {
        result.include("tipoList", Arrays.asList(TipoPacote.values()));
        result.include("moduloList", moduloDAO.listaTudo());
        return dao.carrega(id);
    }

    /**
     *
     * @param diretorio
     */
    public void altera(Diretorio diretorio)
    {
        if (diretorio.getNome()== null || diretorio.getNome().length() < 3)
        {
            validator.add(new ValidationMessage("Nome é obrigatório e precisa ter mais de 3 letras", "produto.nome"));
        }
        validator.onErrorUsePageOf(this).edita(diretorio.getId());

        dao.altera(diretorio);
        result.redirectTo(this).lista(null,null);
    }

    /**
     *
     * @param id
     */
    public void remove(Long id)
    {
        Diretorio diretorio = dao.carrega(id);
        dao.remove(diretorio);
        result.redirectTo(this).lista(null,null);
    }

    /**
     *
     */
    public void formulario()
    {
        result.include("tipoList", Arrays.asList(TipoPacote.values()));
        result.include("moduloList", moduloDAO.listaTudo());
    }
}
