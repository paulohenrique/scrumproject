var Diretorio = {
    initialize: function() {
        $j("#datepickerFrom").datepicker({
            onClose: function(selectedDate) 
            {
                $j("#datepickerTo").datepicker("option", "minDate", selectedDate);
            }

        });
        $j("#datepickerTo").datepicker({
            onClose: function(selectedDate) {
                $j("#datepickerFrom").datepicker("option", "maxDate", selectedDate);
            }
        });
    }
};