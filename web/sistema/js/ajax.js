var Ajax =
        {
            loadingImg: '<img alt="" src="/Atlas/sistema/images/loader.gif">',
            confirmImg: '<img alt="" src="/Atlas/sistema/images/confirm.png">',
            errorImg: '<img alt="" src="/Atlas/sistema/images/error.png">',
            execute: function(param)
            {
                param.beforeSend = param.beforeSend || function() {
                };
                param.success = param.success || function() {
                };
                param.url = param.url || "";
                param.data = param.data || "";

                if (param.url === "" || param.data === "")
                {
                    alert("Atenção! A url e os paramtros são obrigatorios!");
                }
                else
                {
                    param.beforeSend();
                    $j.ajax({
                        url: param.url,
                        type: "POST",
                        data: param.data,
                        success: function(html) {
                            param.success(html);
                        },
                        statusCode: {
                            500: function() {
                                        
                            }
                        }
                    });
                }
            }
        };