var Perfil = {
    showTelaPerfil: function(codPefil, perfil){
        $j.ajax({
            url: "/ovc/perfil/tela?id="+codPefil,
            success: function(data)
            {
                $j('#containerDialogTelas').html(data);
                $j('#containerDialogTelas').dialog({
                    title: "Telas do Perfil " + perfil,
                    modal: true
                });
            }
        });
    },
    initialize: function(){}
};