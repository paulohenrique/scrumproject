var Navigator = {
    form: "form",
    goTo: function (page)
    {
        $j(Navigator.form + " #page").val(page);
        $j(Navigator.form).submit();
    },
    next: function ()
    {
        var pageSize = $j(Navigator.form + " #pageSize").val();
        var total = $j(Navigator.form + " #total").val();
        var lastPage = parseInt(total/pageSize) + 1;
        var page = $j(Navigator.form + " #page").val();
        
        $j(Navigator.form + " #page").val( page == lastPage ? page : parseInt(page) + 1);
        
        $j(Navigator.form).submit();
    },
    prev: function ()
    {
        var page = $j(Navigator.form + " #page").val();
        $j(Navigator.form + " #page").val( page == 1 ? page : parseInt(page) - 1);
        
        $j(Navigator.form).submit();
    }
};