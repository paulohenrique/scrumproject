var Categoria = {
    initialize: function()
    {
        $j('.containerForm form').validate({
            rules: {
                "categoria.nome": {
                    required: true,
                    minlength: 3
                }
            }
        });
    }
};