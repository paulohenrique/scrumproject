<%--
    Document   : edita
    Created on : 05/10/2011, 11:20:04
    Author     : Maison Chaves
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib tagdir="/WEB-INF/tags/" prefix="ovc"%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<ovc:main menu="${true}" title="Editar Historia">
    <ovc:containerForm titulo="Editar Historia" action="../altera">
        <table style="width: 100%">
            <tr>
                <td align="right">
                    <label for="nome">Titulo:</label>
                </td>
                <td>
                    <input id="nome" type="text" name="historia.titulo" value="${historia.titulo}" style="width: 500px" class="inputDefault"/>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <label for="descricao">Descrição:</label>
                </td>
                <td>
                    <textarea id="descricao" name="historia.descricao" style="width: 538px; height: 200px;" class="tinymce">
                        ${historia.descricao}
                    </textarea>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <label for="nome">Pontos:</label>
                </td>
                <td>
                    <input id="nome" type="text" name="historia.pontos" value="${historia.pontos}" style="width: 500px" class="inputDefault"/>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <label for="nome">Prioridade:</label>
                </td>
                <td>
                    <input id="nome" type="text" name="historia.prioridade" value="${historia.prioridade}" style="width: 500px" class="inputDefault"/>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <label for="funcao">Sprint:</label>
                </td> 
                <td>
                    <select id="sprint" name="historia.sprint.id" style="width: 200px" class="inputDefault">
                        <option value="">Sem Sprint</option>
                        <c:forEach items="${sprintList}" var="sprint">
                            <option value="${sprint.id}" <c:if test="${sprint.id == historia.sprint.id}">selected</c:if> >Sprint ${sprint.numero}</option>
                        </c:forEach>
                    </select>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <input type="hidden" name="historia.id" value="${historia.id}" />
                    <input type="hidden" name="historia.projeto.id" value="${historia.projeto.id}" />
                    <div class="toolbar">
                        <button icon="ui-icon-pencil" type="submit">Alterar</button>
                    </div>
                </td>
            </tr>
        </table>
    </ovc:containerForm>
    <script type="text/javascript">
        $j(document).ready(function()
        {
            $j('.containerForm form').validate({
                //errorClass: 'ui-state-error',
                rules: {
                    "historia.nome": {
                        required: true,
                        minlength: 3
                    }
                }
            });
        });
    </script>
</ovc:main>