<%-- 
    Document   : lista
    Created on : 04/10/2011, 17:11:36
    Author     : Maison Chaves
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib tagdir="/WEB-INF/tags/" prefix="ovc"%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<ovc:main menu="${true}" title="Lista de Membros">
    <ovc:containerForm action="lista" titulo="Buscar Membro">
        <table style="width: 100%">
            <tr>
                <td align="right">
                    <label for="nome">Nome:</label>
                </td> 
                <td>
                    <input id="nome" type="text" name="membro.nome" value="${busca.nome}" style="width: 500px" class="inputDefault"/>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <input type="hidden" value="${page}" name="page" id="page" />
                    <input type="hidden" value="${pageSize}" name="pageSize" id="pageSize" />
                    <input type="hidden" value="${total}" name="total" id="total" />
                    <div class="toolbar">
                        <button type="submit" icon="ui-icon-search" >Buscar</button>
                    </div>
                </td>
                
            </tr>
        </table>
    </ovc:containerForm>
    <ovc:containerList lista="${membroList}" titulo="Lista de Membros" vazio="Nenhum membro encontrado!" page="${page}" total="${total}" pageSize="${pageSize}">
        <table cellspacing="0" cellpadding="0" style="width: 100%" class="data">            
            <thead>
                <tr>
                    <th>Nome</th>
                    <th>Papel</th>
                    <th>Horas</th>
                    <th colspan="2">Ações</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${membroList}" var="membro">
                    <tr>
                        <td class="tdData">${membro.nome}</td>
                        <td class="tdData">${membro.funcao.descricao}</td>
                        <td class="tdData">${membro.horas}</td>
                        <td width="25px" align="center" class="actionContainer">
                            <a class="actionList" icon="ui-icon-pencil" href="edita?id=${membro.id}">Editar</a>
                        </td>
                        <td width="25px" align="center" class="actionContainer">
                            <a class="actionList" icon="ui-icon-trash" href="remove?id=${membro.id}">Remover</a>
                        </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </ovc:containerList>
</ovc:main>