<%--
    Document   : edita
    Created on : 05/10/2011, 11:20:04
    Author     : Maison Chaves
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib tagdir="/WEB-INF/tags/" prefix="ovc"%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<ovc:main menu="${true}" title="Editar Menu">
    <ovc:containerForm titulo="Editar Menu" action="altera">
        <table style="width: 100%">
            <tr>
                <td align="right">
                    <label for="nome">Nome:</label>
                </td>
                <td>
                    <input id="nome" type="text" name="menu.nome" value="${itemMenu.nome}" style="width: 500px" class="inputDefault"/>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <label for="link">Link:</label>
                </td>
                <td>
                    <input id="link" type="text" name="menu.link" value="${itemMenu.link}" style="width: 500px" class="inputDefault"/>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <input type="hidden" name="menu.id" value="${itemMenu.id}" />
                    <div class="toolbar">
                        <button icon="ui-icon-pencil" type="submit">Alterar</button>
                    </div>
                </td>
            </tr>
        </table>
    </ovc:containerForm>
    <script type="text/javascript">
        $j(document).ready(function()
        {
            $j('.containerForm form').validate({
                //errorClass: 'ui-state-error',
                rules: {
                    "menu.nome": {
                        required: true,
                        minlength: 3
                    }
                }
            });
        });
    </script>
</ovc:main>