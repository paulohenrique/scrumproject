<%--
    Document   : formulario
    Created on : 05/10/2011, 10:37:14
    Author     : Maison Chaves
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib tagdir="/WEB-INF/tags/" prefix="ovc"%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<ovc:main menu="${true}" javaScript="Sprint" title="Selecionar Membros" >
    <ovc:containerForm titulo="Selecionar Membros" action="membro/seleciona">
        <table style="width: 100%">
            <tr>
                <td align="right">
                    <label for="nome">Time:</label>
                </td>
                <td>
                    <c:forEach items="${membroList}" var="membro" varStatus="i">
                        <c:set var="checked" value='${""}' />

                        <c:forEach items="${sprint.membros}" var="m">
                            <c:if test="${m.id == membro.id}">
                                <c:set var="checked" value='${"checked"}' />
                            </c:if>
                        </c:forEach>

                        <input ${checked} type="checkbox" class="checkDefault" id="membro${membro.id}" name="membros[${membro.id}]" value="${membro.id}"/>
                        <label for="membro${membro.id}">
                            ${membro.nome} - ${membro.funcao.descricao}
                        </label>
                    </c:forEach>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <input type="hidden" name="sprint.projeto.id" value="${projeto.id}" />
                    <div class="toolbar">
                        <button type="reset" icon="ui-icon-cancel" >Cancelar</button>
                        <button type="submit" icon="ui-icon-pencil" >Vincular</button>
                    </div>
                </td>
            </tr>
        </table>
    </ovc:containerForm>
</ovc:main>
