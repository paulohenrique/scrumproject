<%-- 
    Document   : lista
    Created on : 04/10/2011, 17:11:36
    Author     : Maison Chaves
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib tagdir="/WEB-INF/tags/" prefix="ovc"%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<ovc:main menu="${true}" title="Lista de Tarefas">
    <ovc:containerList lista="${tarefaList}" titulo="Lista de Tarefas" vazio="Nenhuma tarefa encontrada!" page="${page}" total="${total}" pageSize="${pageSize}">
        <table cellspacing="0" cellpadding="0" style="width: 100%" class="data">            
            <thead>
                <tr>
                    <th>Descrição</th>
                    <th>Horas</th>
                    <th>Status</th>
                    <th>Atribuido</th>
                    <th colspan="2">Alteração</th>
                    <th colspan="1">Ações</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${tarefaList}" var="tarefa">
                    <tr>
                        <td class="tdData">${tarefa.descricao}</td>
                        <td class="tdData">${tarefa.horas}</td>
                        <td class="tdData">${tarefa.status.descricao}</td>
                        <td class="tdData">${tarefa.membro.nome}</td>
                        <td width="25px" align="center" class="actionContainer">
                            <a class="actionList" icon="ui-icon-search" href="<c:url value="/tarefa/${tarefa.id}/alteracao/lista" />">Controle de Alteração</a>
                        </td>
                        <td width="25px" align="center" class="actionContainer">
                            <a class="actionList" icon="ui-icon-plusthick" href="<c:url value="/tarefa/${tarefa.id}/alteracao/formulario" />">Nova Alteração</a>
                        </td>
                        <td width="25px" align="center" class="actionContainer">
                            <a class="actionList" icon="ui-icon-pencil" href="edita/${tarefa.id}">Editar</a>
                        </td>
                        <!--td width="25px" align="center" class="actionContainer">
                            <a class="actionList" icon="ui-icon-trash" href="remove/${tarefa.id}">Remover</a>
                        </td-->
                    </tr>
                </c:forEach>
            </tbody>
        </table>
        <div class="toolbar" align="center">
            <button type="reset" icon="ui-icon-plusthick" onclick='window.location.href="formulario";'>Nova Tarefa</button>
        </div>
    </ovc:containerList>
</ovc:main>