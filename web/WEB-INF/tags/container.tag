<%--
    Document   : main
    Created on : 16/01/2012, 08:31:37
    Author     : Maisn Chaves
--%>

<%@tag description="put the tag description here" pageEncoding="UTF-8"%>

<%@attribute name="titulo" type="java.lang.String" required="true"%>
<%@attribute name="nome" type="java.lang.String" required="true"%>
<%@attribute name="toggle" type="java.lang.Boolean"%>
<jsp:doBody var="body" />

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="${nome}" style="margin: auto; padding-top: 15px;">
    <div class="container">
        <div class="containerHead" style="height: 28px; padding: 2px; line-height: 28px;">
            <c:if test="${toggle == null || toggle}">
                <a class="toggle" href="javascript:" onclick="$j('.${nome} .containerBody').toggle();">Ocultar</a>
            </c:if>
            ${titulo}
        </div>
        <div class="containerBody">
            ${body}
        </div>
    </div>
</div>

